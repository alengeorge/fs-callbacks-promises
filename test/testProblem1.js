const { createDirectory, createJSON, removeAllJSONs } = require("../problem1");

createDirectory("Json")
  .then(() => createJSON("JSONS", "a.json"))
  .then(() => createJSON("JSONS", "b.json"))
  .then(() => removeAllJSONs("JSONS"))
  .then((res) => console.log(res))
  .catch((err) => console.log(err));

// createJSON('JSONS','a.json').then(createJSON('JSONS','b.json')).then(removeAllJSONs('JSONS'))
