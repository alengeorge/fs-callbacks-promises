const {
  convertToLower,
  convertToUpper,
  readNewFilesSort,
  readFile,
  deleteFilesIn,
} = require("../problem2");

// readFile('lipsum.txt')

convertToUpper("UpperCase.txt")
  .then(() => convertToLower("LowerCase.txt"))
  .then(() => readNewFilesSort("UpperCase.txt", "LowerCase.txt"))
  .then(()=>readFile("filename.txt")).then((data)=>deleteFilesIn(data))
  .then(() => deleteFilesIn("filename.txt"));


// readFile("filename.txt").then((data)=>deleteFilesIn(data))