const fs = require("fs");

const readFile = (filename) => {
  return new Promise((res, rej) => {
    fs.readFile(filename, "utf8", (err, data) => {
      if (err) {
        rej(err);
      } else {
        console.log("Reading: " + filename);
        res(data);
      }
    });
  });
};

function writeFile(filename, data) {
  return new Promise((res, rej) => {
    fs.writeFile(filename, data, (err) => {
      if (err) rej(err);
      else {
        res(console.log(filename + " updated successfully\n"));

        // callback('filename.txt','sorted.txt')
      }
    });
  });
}

function updateFile(filename, data) {
  return new Promise((res, rej) => {
    fs.appendFile(filename, data+' ' , (err) => {
      if (err) {
        rej(err);
      } else {
        res(console.log(`\n${filename} updated`));
      }
    });
  });
}

function convertToUpper(filename) {
  return readFile(filename)
    .then((data) => writeFile("UpperCase.txt", data.toUpperCase()))
    .then(updateFile("filename.txt", filename))
    .catch((err) => console.log(err));
}

function convertToLower(filename) {
  return readFile("lipsum.txt")
    .then((data) =>
      writeFile(filename, data.toLowerCase().split(".").join("\n"))
    )
    .then(updateFile("filename.txt", filename))
    .catch((err) => console.log(err));
}

function readNewFilesSort(fileName1, filename2) {
  return readFile(fileName1)
    .then((data) => writeFile("sorted.txt", data))
    .then(() => readFile(filename2))
    .then((data) => writeFile("sorted.txt", data))
    .then(() => readFile("sorted.txt"))
    .then((data) => writeFile("sorted.txt", data.split("\n").sort().join()))
    .then(()=> updateFile("filename.txt", "sorted.txt"))
    .catch((err) => console.log(err));
}

// function deleteFile(filename) {
//   return new Promise((res, rej) => {
//     fs.unlink(filename, (err) => {
//       if (err) {
//         rej(err);
//       } else {
//         res(console.log(file + " got deleted"));
//       }
//     });
//   });
// }

function deleteFilesIn(data) {
  return new Promise((res,rej)=>{
    let files = data.split(" ")
    files.forEach((filename) => {
      // console.log(element+" deleted")
      let count=0
      if(!filename){
            fs.unlink(filename, (err) => {
        if (err) {
          rej(err);
        } else {
          count+=1
          if (count== files.length)
          res(console.log(filename + " got deleted"));
        }
      })
    }
     
    });
  });
}
/*
function deleteAll(dirName) {
  return new Promise((res, rej) => {
    fs.readdir(dirName, (err, files) => {
      let count = 0;
      if (err) {
        rej(err);
      } else {
        files.forEach((file) => {
          count++
          deleteFile(file);
          if (count == files.length) {
            res(console.log("All files deleted"));
          }

          
        });
      }
    });
  });
}
*/
module.exports = {
  convertToUpper,
  convertToLower,
  readNewFilesSort,
  readFile,
  deleteFilesIn,
};
