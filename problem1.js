const { dir } = require("console");
const fs = require("fs");

function createDirectory(dirName) {
  return new Promise((res, rej) => {
    fs.mkdir(dirName, (err, path) => {
      if (err) {
        rej(err);
      } else {
        res(console.log(dirName + "Directory created"));
      }
    });
  });
}

function createJSON(dirName, JSONname) {
  return new Promise((res, rej) => {
    fs.writeFile(dirName + "/" + JSONname, "{Hi:Hello}", (err) => {
      if (err) {
        rej(err);
      } else {
        res(console.log(JSONname + "file created"));
      }
    });
  });
}

function removeAllJSONs(dirName) {
  return new Promise((res, rej) => {
    fs.readdir(dirName, (err, files) => {
      if (err) rej(err);
      else {
        let count = 0;
        files.forEach((file) =>
          fs.unlink(dirName + "/" + file, (err) => {
            if (err) {
              rej(err);
            } else {
              count += 1;
              if (count == files.length) {
                res("All files deleted");
              }
              console.log(file + " is deleted");
             
            }
          })
        );
      }
    });
  });
}

module.exports = { removeAllJSONs, createDirectory, createJSON };
